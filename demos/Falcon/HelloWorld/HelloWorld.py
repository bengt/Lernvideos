import sys

import falcon

print(b"Got called by" + sys.executable)

class GreeterResource:
    def on_get(self, req, resp, user_id):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200
        resp.body = 'Hello ' + user_id + '!'

wsgi_app = api = falcon.API()
greeter_resource = GreeterResource()
api.add_route('/hello/{user_id}', greeter_resource)


# Installing Dependencies

    sudo python3-pip install --upgrade -r requirements.txt

# Running

    uwsgi --enable-threads --http :8080 --wsgi-file HelloWorld.py --callable wsgi_app


# Testing

    http://localhost:8080/things

should display

    Hello world!

# Benchmarking

    ab -n 1000 -c 2 http://localhost:8080/hello/world


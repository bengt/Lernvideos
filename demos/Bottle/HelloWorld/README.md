# Installing Requirements

    sudo python3-pip install --upgrade -r requirements.txt

# Running

    python3 HelloWorld.py

# Testing

<http://localhost:8080/hello/world> should return:

    <b>Hello world</b>!
    
# Benchmarking

    ab -n 1000 -c 2 http://localhost:8080/hello/world


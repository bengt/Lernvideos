Projektziel

-   siehe Anforderungsdokument: Vision

Aufgabenstellung

-   siehe Anforderungsdokument: Spezifische Anforderungen/Funktionale Anforderungen/Video Einstellen bzw. Ansehen
-   siehe Anforderungsdokument: Prozess- und Dokumentationsanforderungen
-   Siehe Anforderungsdokument: Funktionale Anforderungen/Erweiterarkeit

Negative Anforderungen

-   siehe Anforderungsdokument: Produktabgrenzung

Prozessanforderungen

-   siehe Anforderungsdokument: Prozessanforderungen

Kategorien/Hierarchie

-   siehe Anforderungsdokument: Domänenmodell

Parallelhierachie

-   siehe Anforderungsdokument: Domänenmodell

Metadaten

-   Siehe Anforderungsdokument: Domänenmodell/Hierarchie
-   Siehe Anforderungsdokument: Domänenmodell/Videodokumente

Zugriffskontrolle

-   Siehe Anforderungsdokument: Funktionale Anforderungen/Rechtemanagement

Zentrale Anwendungsfälle

-   Siehe Anforderungsdokument: Anwendungsfälle
-   Siehe Anforderungsdokument: Spezifische Anforderungen/Funktionale Anforderungen

Videos betrachten

-   Siehe Anforderungsdokument: Projektangelegenheiten/Lösungsideen
-   Siehe Anforderungsdokument: Spezifische Anforderungen/Funktionale Anforderungen/Videos Browsen
-   Siehe Anforderungsdokument: Hierarchie/Filter
-   Siehe Anforderungsdokument: Produktabgreunzung/Soziale Features

Video hochladen

-   Siehe Anforderungsdokument: Anwendungsfsälle/Video Einstellen

Review-Prozess / Freigabemöglichkeit durch Lehrer

-   Siehe Anforderungsdokument: Funktionale Anforderungen/Abnahme-Prozess
-   Siehe Anforderungsdokument: Anforderungen an Nutzer/Lehrer
-   Siehe Anforderungsdokument: Anwendungsfälle/eingestellte Videos anzeigen

Zensurmöglichkeit durch Admin

-   Siehe Anforderungsdokument: Funktionale Anforderungen/Benachrichtigungen
-   Siehe Anforderungsdokument: Funktionale Anforderungen/Reports
-   Siehe Anforderungsdokument: Funktionale Anforderungen/Abnahme-Prozess
-   Siehe Anforderungsdokument: Anwendungsfälle/Video browsen
-   Siehe Anforderungsdokument: Anwendungsfälle/Video browsen
    -   Siehe Anforderungsdokument: Anforderungen an Nutzer/Lehrer bzw. Adminstratoren

Vandalismus

-   in Grundschule
    -   Siehe Anforderungsdokument: Anforderungen Nutzer/Lehrer
    -   Siehe Anforderungsdokument: Anwendungsfälle/Videos einstellen
    -   Siehe Anforderungsdokument: Anwendungsfälle/Video einstellen
    -   Siehe Anforderungsdokument: Anwendungsfälle/Video freigeben
    -   Siehe Anforderungsdokument: Anforderungen Nutzer/Lehrer
-   auf Gynmnasial-Niveau
    -   vielleicht ganz andere Abschottungsanforderungen
    -   Lösungsidee: austauschbare Anmeldemodule

Anforderungen an Nutzer

-   Googlen kann als Fähigkeit vorausgesetzt werden
-   Neue Fähigkeiten sprechen sich herum
-   Lesen der Fachtitel kann vorausgesetzt werden
-   Fähigkeit zur Tabletbenutzung kann vorausgesetzt werden
-   Bis auf weiteres können wir Schüler als normale Nutzer ansehen

Usability-Anforderungen

-   Schnelle Eingabe durch Admin
-   Intuitive Benutzbarkeit sollte gegeben sein  
    durch alle Nutzer (inkl. Schüler)
-   User Interface ist nicht unser Fokus
-   Abschätzungen
    -   Video machen ist schon schwierig genug;  
        mit dem Hochladen kommt der Nutzer dann auch klar
    -   Tobis 1 Lernsoftware Tobi ist komplex zu bedienen;  
        Schüler kommen damit klar

Evaluation

-   Mit Benutzern (und evtl. dem Prototypen)
    -   ist möglich, wenn wir das für unbedingt nötig halten
    -   ist eigentlich nicht vorgesehen
    -   kurz nein
-   statt dessen
    -   Lasttest wäre zu machen
    -   Größenordnung der Benutzung abschätzen

Infrastruktur-Voraussetzungen

-   eigenständiger Ubuntu-Server mit unseren System drauf
-   Eventuell nur als Virtuelle Maschine
-   Port 80 ist frei für uns
-   Physikalisch selbes Netz wie Anzeigegeräte
-   Idee: Veröffentlichen Spiegel im Netz, der nur gelesen werden darf
-   iServ würde als Gateway in der Schule stehen, ist eher nicht zu benutzen

Integration / Releases

-   in/auf Schul-Infrastruktur
-   Regelmäßiges Deployment ist nicht gewünscht
-   Lauffähige Version reicht als Release

Webinterface

-   definitive Anforderung
-   kommt aus Aufgabenstellung

Aktualität der Website

-   Für den Browser muss HTML erzeugt werden.
-   Statische Lösung
    -   HTML-Seiten regelmäßig erzeugen
    -   erzeugte HTML-Seiten als statische Datei an Clients ausliefern 
    -   Build einmal am Tag ist ausreichend
-   Dynamische Lösung
    -   (z.B. Python)-Web-Server stellt API für Zugriff auf Reports, Ansichten etc.
    -   Javascript-API-Client rendert HTML aus Antwort der API
    -   auch möglich
    -   Native App ist für die Zukunft denkbar, aber nicht geplant
    -   Klare Trennung von
        -   Model ((z.B. JSON-)-Antworten)
        -   View (Javascript-Client)
        -   Controller ((z.B. Python)-Server)

Konvertierung

-   kann eventuell erst mal weggelassen werden
-   langsam ist tolerierbar, wegen Review-Prozess

-   Selbsteinarbeitung in Videoformate für das Projekt
-   Problem: Videobitrate lässt sich nicht vorher bestimmen
    -   Idee: nach YouTube richten, 720p-Modus nachbauen
    -   Idee: Bandbreite an Videoinhhalt anpassen  
        (Viele Details? Große Ausgangsauflösung?)
    -   Idee: Wir sondieren experimentell
        -   System erst mal ohne Konvertierung aufbauen
        -   Videos verschiedener Auflösungen/Bitraten ins System bringen
        -   Abspielen von Videos über angepeilte Endgeräte und Medien testen
        -   Beispiele für Testfälle:
            -   450 Mbit/s WLAN mit 4 Endgeräten?
            -   54 Mbit/s WLAN mit 4 Endgeräten?
    -   Idee: State-Of-The-Art-Analyse
        -   Vergleichbare Plattformen (YouTube, Vimeo, myVideo etc.) betrachten
        -   Video-Formate auswerten
        -   Im Zweifelsfall machen, was "die Großen" machen
    -   Video-Bitraten sind durch Abschätzung festzulegen
-   Video-Länge ist auf kurze Videos begrenzt
    -   Weil Schüler zum Produzieren längerer Videos ein Skript bräuchten
-   auf MP4 normalisieren
-   auf Nexus 7 auslegen  
    (also Chrome-Browser & WebM)
-   1280 Breite reicht für Tablets und alle absehbaren Beamer
-   WLAN
    -   kann als ausreichend angenommen werden
    -   wird evtl durch Hardware gelöst
    -   Nicht unser Problem.
-   Konvertieren in der Nacht kann als Notiz ins Handbuch

Architektur

-   Server
    -   Nur Videos + Metadaten im System, keine sonstigen Dokumente
    -   System ist mindestens teilweise dateibasiert  
        weil Video-Daten notwendiger Weise im Dateisystem liegen
        am einfachsten können Metadaten ebenfalls dateibasiert verwaltet werden
-   Client
    -   rein statische Lösung ist erlaubt
    -   API + JS-Client ist erlaubt  
        Kostenlose App-Freundlichkeit
    -   [CouchApp](http://couchapp.org/page/index) + JS-Client ist erlaubt  
        Kostenlose App-Freundlichkeit + kostenlose Replikation
    -   Maßgabe: Macht was ihr gut könnt und was modern ist.

Programmiersprache

-   Ruby, Python oder was wir können
    -   Rechtzeitig sich auf eine Version festlegen
-   Node.js wäre vielleicht sinnvoll einsetzbar
-   Java braucht dann Tomcat, ist komplex zu administrieren

Offene Fragen

-   Soll es möglich sein, urheberrechtlich geschütztes Material als solches zu kennzeichnen?  
    (z.B. Aufgaben aus Lehrbüchern)
-   Soll es möglich sein, Videos für nur eine Klasse freizugeben?
-   Soll das System eine Lizenzierung der Videos ermöglichen (Creative Commons)
    -   Problem: Rechtsmündigkeit bei den Schülern ist nicht gegeben.
    -   Lösung: Eltern Videos zur Weiterverwendung freigeben lassen


# Entwicklersysteme

Systeme, die uns zum Entwickeln zur Verfügung stehen.

System | vorhanden bei Bengt | vorhanden bei Dennis | Summe
------ | ------------------- | -------------------- | -----
Windows | 1 | 0 | 1
Linux | 0 | 0 | 0
BSD | 2 | 0 | 2
Mac OS | 4 | 2 | 4
Android | 2 | 3? | 5
iOS | 4 | 4 | 8
Windows Phone | 4 | 2.5 | 6.5

Dennis - Windows Phone: Eine virtuelle Maschine bzw. Simulator ist bei Windows Phone entwicklungstools (kostenlos wegen Dreamspark premium) verfügbar.
Dennis - Android: Es gibt den Versuch Adroid auf x86 und damit einer virtuellen Maschine zu portierten. Ich kann jedoch den Projektstand und -fortschritt schlecht einschätzen.
(Ich frage mal bei Leuten aus dem Anroid Praktikum nach wie die es dort lösen)

## Legende

Zahl | Bedeutung
--- | ---
4 | praktisch nicht verfügbar (System müsste angeschafft werden)
2 | nur mit großem Aufwand verfügbar (z.B. fremndes System in der ARBI müsste benutzt werden)
1 | nur mit erhöhtem Aufwand verfügbar (z.B. zweiter stationärer Rechner müsste benutzt werden)
0 | direkt verfügbar (primäres Betriebssystem bzw. VM auf primärem System vorhanden)

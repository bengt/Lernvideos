-   nicht direkt eLearning, sondern schulrelevanz
-   echte Kunden
-   Kompetenzen der Schule zur Weiterentwicklung nötig
-   innovative Projekte, Dinge die es noch nicht gibt
-   Ziel: Auseinandersetzung mit Gegebenheiten Integration in Schulumfeld

Aufgabe Testdatensatz

-   Suchmaschine / Portal / Inhaltsbasierter oder Kollaborativer Recommender
-   Automatisierte Unterstützung der Unterichtsvorberietung
-   System besteht
-   Einschätzung der Lerninhalte von einem Arbeitsblatt

Aufgabe Crawler

-   Crawler, der sowas wie ein Naive-Bayes-Klassifier  macht
-   Merkmale ?! <-> Lernziele

Aufgabe Lernvideoserver

-   Video + Metadaten
-   Index, Player
-   Grund-Schul-Lehrer / -Schüler tauglich

Tipps

-   Wiki im Studip für Doku benutzen
-   kunden befragen
-   RE machen
-   Tools finden
-   System implementieren

Prüfungsleistungen

-   Zusammenarbeit
-   Präsentation
-   System


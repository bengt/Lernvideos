<br/>
<br/>
<br/>
<br/>
<center><span id="claim">Bengt Lüers, Dennis Kregel</span></center>
<h1>Schul-Video-Sharing-Plattform</h1>
<span id="claim">Abschlussbericht</span>

## Übersicht

-   fertiges Anforderungsdokument
-   Zusammenfassung vom Projekt
-   finaler Prototyp
-   Fazit

## Wiederholung: Motivation

-   Unterstützung von Lehrern im Unterricht
    -   durch Multimodalität von Videos
-   Unterstützung von Schülern beim selbstgeleiteten Lernen
    -   durch Wiederholbarkeit

## Zuvor im Anforderungsdokument

-   inhaltlich stabil
-   letzte Änderungen stehen noch aus

## Neu im Anforderungsdokument: Benutzerhandbücher

Schülerhandbuch

-   Videos Browsen
-   Video Abspielen
-   Einloggen
-   Video Hochladen

Lehrerhandbuch

-   Video Editieren
-   Videos Freigeben

Administratorhandbuch

-   System Aufsetzen
-   Freigaben Widerrufen

## Anforderungsdokument: Überblick

-   3 Seiten Kapitel 1 - Einleitung
-   26 Seiten Kapitel 2 - Allgemeine Beschreibung
    -   22 Seiten Anwendungsfälle
-   6 Seiten Kapitel 3 - Spezifische Anforderungen
-   2 Seiten Kapitel 4 - Projektangelegenheiten
-   9 Seiten Kapitel 5 - Anhang

## Neue Komponente: Importer

Aufgabe

-   Automatisches Importieren von Videos aus SMB-Freigabe.

Verworfenes

-   Mediacore API umfasst Upload nicht
-   Frontend zu automatisieren schien zu aufwändig

Lösung

-   für Upload existierte Funktionstest
-   modfiziert, um Daten von iNotify anzunehmen
-   iNotify-Watchdog für SMB-Verzeichnis

## Demo/Video für zentrale Anwendungsfälle

-   Alt + Tab

##  Leistungstest

-   ca. 5 Seiten/s auf diesem Laptop 

## Fazit: XP mit GitHub

Velocity hat stark geschwankt

&plus; früh angefangen<br/>
&minus; nicht durchgehalten<br/>
&plus; Crunchtime hat funktioniert

Einbindung vom Auftraggeber war durchwachsen

&plus; Anforderungsdokument hat Missverständnisse sofort aufgedeckt<br/>
&minus; früher wäre besser gewesen<br/>

Planung wurde mehrfach umgeworfen

&minus; mangelnde Meilenstein-Disziplin<br/>
&plus; es gibt Planung<br/>
&plus; GitHub macht Umplanung leicht<br/>

## Fazit: Getroffene Entscheidungen

Mediacore

-   gute Modifizierbarkeit
    -   überschaubare Komplexität
    -   gute Strukturierung
-   recht komplett für eine 1-Mann-Projekt
    -   taugt durchaus als Videoplattform mit Release-Workflow

Ninja-IDE

-   gute Code-Navigation
-   kam nicht in die Quere

Python

-   Templating mit Logik ist krude

## Zusammenfassung

-   Aufgabestellung erarbeitet
    -   Interviews mit Auftraggeber
    -   Anforderungsdokument diskutiert
-   Lösungswege erarbeitet
    -   systematisch nach Standardsoftware gesucht
-   zentrale Anforderungen erfüllt
    -   gleich mehr

## Statistik

-   **101 Commits**
-   **97 Issues**
    -   48 Closed
    -   49 Open  
        (meist "unaufgeräumte" Anwendungsfälle und User-Stories)
-   Average Velocity of **4 Closed Issues per Week**
    -   2 pro Person und Woche
    -   2 * 1 bis 3 h = 2 bis 6 SWS
-   Anforderungsdokument
    -   1500 Zeilen
    -   7000 Wörter
    -   55 KB
    -   50 Seiten

<!-- -   GitHub Graphs -->

## Ende

<br/>
<br/>
<br/>
<center>Vielen Dank für die Aufmerksamkeit!</center>

## Tools

-   Präsentations-Framework: [reveal.js](https://github.com/hakimel/reveal.js/)
-	Präsenstations-Stil: [Neoskop Light](https://github.com/neoskop/revealjs-neoskop-theme)


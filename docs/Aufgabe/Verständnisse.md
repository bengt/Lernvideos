Die Aufgabe, wie verschiedene Leute sie verstanden haben in einem Satz:

-   Bengt: Kinder abfilmen, Videos anderen Kindern zu Verfügung stellen.
-   Nastja: Videos aggregieren Kindern und Lehrern zu Verfügung stellen.
-   Klaas: Videos aus verschiedenen Quellen nehmen, Format normalisieren, Index generieren, vornehmlich Lehrern auch Kindern zur Verfügung stellen.
-   Dennis: Videos von fremden Websites sollen falls möglich eingebunden und ansonsten heruntergeladen und wieder bereitgestellt werden.

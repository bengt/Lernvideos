# SMB Importer

Imports Videos shared via SMB or similar mechanisms.

## Dependencies

    $ [sudo] pip install -r requirements.txt

## Setup

Mount SMB-Share, for example using gvfs:

    $ gvfs-mount smb://server/metadata
    $ cd /run/user/<USERNAME>/gvfs/server/metadata

## Running

    $ python SmbImporter.py


-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 31. Okt 2013 um 02:31
-- Server Version: 5.5.33a-MariaDB
-- PHP-Version: 5.5.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `mediacore`
--
DROP DATABASE `mediacore`;
CREATE DATABASE `mediacore` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mediacore`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `parent_id` (`parent_id`),
  KEY `ix_categories_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Daten für Tabelle `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `parent_id`) VALUES
(3, 'Mathematik', 'mathematik', 7),
(4, 'Deutsch', 'deutsch', 7),
(5, 'Musik', 'musik', 7),
(6, 'Kunst', 'kunst', 7),
(7, 'Klasse 1', 'klasse-1', NULL),
(8, 'Klasse 3', 'klasse-3', NULL),
(9, 'Mathematik', 'mathematik-2', 8),
(10, 'Deutsch', 'deutsch-2', 8),
(11, 'Kunst', 'kunst-2', 8),
(12, 'Musik', 'musik-2', 8);

--
-- Trigger `categories`
--
DROP TRIGGER IF EXISTS `categories_ad`;
DELIMITER //
CREATE TRIGGER `categories_ad` AFTER DELETE ON `categories`
 FOR EACH ROW BEGIN
	UPDATE media_fulltext
		SET categories = TRIM(', ' FROM REPLACE(
			CONCAT(', ', categories,   ', '),
			CONCAT(', ', OLD.name, ', '),
			', '))
		WHERE media_id IN (SELECT media_id FROM media_categories
			WHERE category_id = OLD.id);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `categories_au`;
DELIMITER //
CREATE TRIGGER `categories_au` AFTER UPDATE ON `categories`
 FOR EACH ROW BEGIN
	IF OLD.name != NEW.name THEN
		UPDATE media_fulltext
			SET categories = TRIM(', ' FROM REPLACE(
				CONCAT(', ', categories,   ', '),
				CONCAT(', ', OLD.name, ', '),
				CONCAT(', ', NEW.name, ', ')))
			WHERE media_id IN (SELECT media_id FROM media_categories
				WHERE category_id = OLD.id);
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `reviewed` tinyint(1) NOT NULL,
  `publishable` tinyint(1) NOT NULL,
  `author_name` varchar(50) NOT NULL,
  `author_email` varchar(255) DEFAULT NULL,
  `author_ip` bigint(20) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_id` (`media_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `comments`
--

INSERT INTO `comments` (`id`, `media_id`, `subject`, `created_on`, `modified_on`, `reviewed`, `publishable`, `author_name`, `author_email`, `author_ip`, `body`) VALUES
(1, 1, 'Re: New Media', '2013-10-30 21:19:35', '2013-10-30 21:19:35', 0, 0, 'John Doe', NULL, 2130706433, '<p>Hello to you too!</p>');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(16) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`, `display_name`, `created`) VALUES
(1, 'admins', 'Admins', '2013-10-30 21:19:35'),
(2, 'lehrer', 'Lehrer', '2013-10-30 21:19:35'),
(3, 'anonymous', 'Everyone (including guests)', '2013-10-30 21:19:35'),
(4, 'schueler', 'Schüler', '2013-10-30 21:19:35');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `groups_permissions`
--

DROP TABLE IF EXISTS `groups_permissions`;
CREATE TABLE IF NOT EXISTS `groups_permissions` (
  `group_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  KEY `group_id` (`group_id`),
  KEY `permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `groups_permissions`
--

INSERT INTO `groups_permissions` (`group_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(3, 3),
(3, 4),
(3, 5),
(4, 3),
(4, 4),
(4, 5),
(2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(8) DEFAULT NULL,
  `slug` varchar(50) NOT NULL,
  `podcast_id` int(11) DEFAULT NULL,
  `reviewed` tinyint(1) NOT NULL,
  `encoded` tinyint(1) NOT NULL,
  `publishable` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `publish_on` datetime DEFAULT NULL,
  `publish_until` datetime DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` text,
  `description_plain` text,
  `notes` text,
  `duration` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL,
  `popularity_points` int(11) NOT NULL,
  `popularity_likes` int(11) NOT NULL,
  `popularity_dislikes` int(11) NOT NULL,
  `author_name` varchar(50) NOT NULL,
  `author_email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `podcast_id` (`podcast_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `media`
--

INSERT INTO `media` (`id`, `type`, `slug`, `podcast_id`, `reviewed`, `encoded`, `publishable`, `created_on`, `modified_on`, `publish_on`, `publish_until`, `title`, `subtitle`, `description`, `description_plain`, `notes`, `duration`, `views`, `likes`, `dislikes`, `popularity_points`, `popularity_likes`, `popularity_dislikes`, `author_name`, `author_email`) VALUES
(1, NULL, 'new-media', NULL, 1, 0, 0, '2013-10-30 21:19:35', '2013-10-30 21:19:35', NULL, NULL, 'New Media', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, 0, 0, 0, 0, 0, 0, 0, 'Admin', 'admin@somedomain.com'),
(2, 'video', 'workflow-in-mediacore', NULL, 1, 1, 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '2010-05-13 02:29:40', NULL, 'Workflow in MediaCore', NULL, '<p>This sceencast explains the publish status feature in MediaCore.</p><p>Initially all videos uploaded through the front-end or admin panel are placed under &quot;awaiting review&quot; status. Once the administrator hits the &quot;review complete&quot; button, they can upload media. Videos can be added in any format, however, they can only be published if they are in a web-ready format such as FLV, M4V, MP3, or MP4. Alternatively, if they are published through Youtube or Vimeo the encoding step is skipped</p><p>Once uploaded and encoded the administrator can then publish the video.</p>', 'This sceencast explains the publish status feature in MediaCore.\nInitially all videos uploaded through the front-end or admin panel are placed under "awaiting review" status. Once the administrator hits the "review complete" button, they can upload media. Videos can be added in any format, however, they can only be published if they are in a web-ready format such as FLV, M4V, MP3, or MP4. Alternatively, if they are published through Youtube or Vimeo the encoding step is skipped\nOnce uploaded and encoded the administrator can then publish the video.', NULL, 218, 0, 0, 0, 0, 0, 0, 'MediaCore Team', 'info@mediacore.com'),
(3, 'video', 'creating-a-podcast-in-mediacore', NULL, 1, 1, 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '2010-05-13 02:33:44', NULL, 'Creating a Podcast in MediaCore', NULL, '<p>This describes the process an administrator goes through in creating a podcast in MediaCore. An administrator can enter information that will automatically generate the iTunes/RSS feed information. Any episodes published to a podcast will automatically publish to iTunes/RSS.</p>', 'This describes the process an administrator goes through in creating a podcast in MediaCore. An administrator can enter information that will automatically generate the iTunes/RSS feed information. Any episodes published to a podcast will automatically publish to iTunes/RSS.', NULL, 100, 0, 0, 0, 0, 0, 0, 'MediaCore Team', 'info@mediacore.com'),
(4, 'video', 'adding-a-video-in-mediacore', NULL, 1, 1, 1, '2013-10-30 21:19:35', '2013-10-31 03:29:43', '2010-05-13 02:37:36', NULL, 'Adding a Video in MediaCore', NULL, '<p>This screencast shows how video or audio can be added in MediaCore.</p><p>MediaCore supports a wide range of formats including (but not limited to): YouTube, Vimeo, Amazon S3, Bits on the Run, BrightCove, Kaltura, and either your own server or someone else''s.</p><p>Videos can be uploaded in any format, but can only be published in web-ready formats such as FLV, MP3, M4V, MP4 etc.</p>', 'This screencast shows how video or audio can be added in MediaCore.\nMediaCore supports a wide range of formats including (but not limited to): YouTube, Vimeo, Amazon S3, Bits on the Run, BrightCove, Kaltura, and either your own server or someone else''s.\nVideos can be uploaded in any format, but can only be published in web-ready formats such as FLV, MP3, M4V, MP4 etc.', NULL, 169, 3, 0, 0, 0, 0, 0, 'MediaCore Team', 'info@mediacore.com');

--
-- Trigger `media`
--
DROP TRIGGER IF EXISTS `media_ad`;
DELIMITER //
CREATE TRIGGER `media_ad` AFTER DELETE ON `media`
 FOR EACH ROW BEGIN
	DELETE FROM media_fulltext WHERE media_id = OLD.id;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `media_ai`;
DELIMITER //
CREATE TRIGGER `media_ai` AFTER INSERT ON `media`
 FOR EACH ROW BEGIN
	INSERT INTO media_fulltext
		SET `media_id` = NEW.`id`,
		    `title` = NEW.`title`,
		    `subtitle` = NEW.`subtitle`,
		    `description_plain` = NEW.`description_plain`,
		    `notes` = NEW.`notes`,
		    `author_name` = NEW.`author_name`,
		    `tags` = '',
		    `categories` = '';
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `media_au`;
DELIMITER //
CREATE TRIGGER `media_au` AFTER UPDATE ON `media`
 FOR EACH ROW BEGIN
	UPDATE media_fulltext
		SET `media_id` = NEW.`id`,
		    `title` = NEW.`title`,
		    `subtitle` = NEW.`subtitle`,
		    `description_plain` = NEW.`description_plain`,
		    `notes` = NEW.`notes`,
		    `author_name` = NEW.`author_name`
		WHERE media_id = OLD.id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `media_categories`
--

DROP TABLE IF EXISTS `media_categories`;
CREATE TABLE IF NOT EXISTS `media_categories` (
  `media_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`media_id`,`category_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `media_categories`
--

INSERT INTO `media_categories` (`media_id`, `category_id`) VALUES
(4, 6),
(4, 7),
(4, 8),
(4, 9);

--
-- Trigger `media_categories`
--
DROP TRIGGER IF EXISTS `media_categories_ad`;
DELIMITER //
CREATE TRIGGER `media_categories_ad` AFTER DELETE ON `media_categories`
 FOR EACH ROW BEGIN
	UPDATE media_fulltext
		SET categories = TRIM(', ' FROM REPLACE(
			CONCAT(', ', categories, ', '),
			CONCAT(', ', (SELECT name FROM categories WHERE id = OLD.category_id), ', '),
			', '))
		WHERE media_id = OLD.media_id;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `media_categories_ai`;
DELIMITER //
CREATE TRIGGER `media_categories_ai` AFTER INSERT ON `media_categories`
 FOR EACH ROW BEGIN
	UPDATE media_fulltext
		SET categories = CONCAT(categories, ', ', (SELECT name FROM categories WHERE id = NEW.category_id))
		WHERE media_id = NEW.media_id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `media_files`
--

DROP TABLE IF EXISTS `media_files`;
CREATE TABLE IF NOT EXISTS `media_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `type` varchar(16) NOT NULL,
  `container` varchar(10) DEFAULT NULL,
  `display_name` varchar(255) NOT NULL,
  `unique_id` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `bitrate` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_id` (`media_id`),
  KEY `storage_id` (`storage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `media_files`
--

INSERT INTO `media_files` (`id`, `media_id`, `storage_id`, `type`, `container`, `display_name`, `unique_id`, `size`, `created_on`, `modified_on`, `bitrate`, `width`, `height`) VALUES
(1, 2, 2, 'video', 'mp4', 'tutorial-workflow-in-mediacore.mp4', 'http://mediacorecommunity.org/files/videos/tutorial-workflow-in-mediacore.mp4', NULL, '2010-05-13 02:29:40', '2013-10-30 21:19:35', NULL, NULL, NULL),
(2, 3, 2, 'video', 'mp4', 'tutorial-create-podcast-in-mediacore.mp4', 'http://mediacorecommunity.org/files/videos/tutorial-create-podcast-in-mediacore.mp4', NULL, '2010-05-13 02:33:44', '2013-10-30 21:19:35', NULL, NULL, NULL),
(3, 4, 2, 'video', 'mp4', 'tutorial-add-video-in-mediacore.mp4', 'http://mediacorecommunity.org/files/videos/tutorial-add-video-in-mediacore.mp4', NULL, '2010-05-13 02:37:36', '2013-10-30 21:19:35', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `media_files_meta`
--

DROP TABLE IF EXISTS `media_files_meta`;
CREATE TABLE IF NOT EXISTS `media_files_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `media_files_id` (`media_files_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `media_fulltext`
--

DROP TABLE IF EXISTS `media_fulltext`;
CREATE TABLE IF NOT EXISTS `media_fulltext` (
  `media_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description_plain` text,
  `notes` text,
  `author_name` varchar(50) NOT NULL,
  `tags` text,
  `categories` text,
  PRIMARY KEY (`media_id`),
  FULLTEXT KEY `media_fulltext_admin` (`title`,`subtitle`,`tags`,`categories`,`description_plain`,`notes`),
  FULLTEXT KEY `media_fulltext_public` (`title`,`subtitle`,`tags`,`categories`,`description_plain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `media_fulltext`
--

INSERT INTO `media_fulltext` (`media_id`, `title`, `subtitle`, `description_plain`, `notes`, `author_name`, `tags`, `categories`) VALUES
(1, 'New Media', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, 'Admin', NULL, ''),
(2, 'Workflow in MediaCore', NULL, 'This sceencast explains the publish status feature in MediaCore.\nInitially all videos uploaded through the front-end or admin panel are placed under "awaiting review" status. Once the administrator hits the "review complete" button, they can upload media. Videos can be added in any format, however, they can only be published if they are in a web-ready format such as FLV, M4V, MP3, or MP4. Alternatively, if they are published through Youtube or Vimeo the encoding step is skipped\nOnce uploaded and encoded the administrator can then publish the video.', NULL, 'MediaCore Team', NULL, ''),
(3, 'Creating a Podcast in MediaCore', NULL, 'This describes the process an administrator goes through in creating a podcast in MediaCore. An administrator can enter information that will automatically generate the iTunes/RSS feed information. Any episodes published to a podcast will automatically publish to iTunes/RSS.', NULL, 'MediaCore Team', NULL, ''),
(4, 'Adding a Video in MediaCore', NULL, 'This screencast shows how video or audio can be added in MediaCore.\nMediaCore supports a wide range of formats including (but not limited to): YouTube, Vimeo, Amazon S3, Bits on the Run, BrightCove, Kaltura, and either your own server or someone else''s.\nVideos can be uploaded in any format, but can only be published in web-ready formats such as FLV, MP3, M4V, MP4 etc.', NULL, 'MediaCore Team', NULL, ', Klasse 1, Klasse 3, Kunst, Mathematik');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `media_meta`
--

DROP TABLE IF EXISTS `media_meta`;
CREATE TABLE IF NOT EXISTS `media_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `media_id` (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `media_tags`
--

DROP TABLE IF EXISTS `media_tags`;
CREATE TABLE IF NOT EXISTS `media_tags` (
  `media_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`media_id`,`tag_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Trigger `media_tags`
--
DROP TRIGGER IF EXISTS `media_tags_ad`;
DELIMITER //
CREATE TRIGGER `media_tags_ad` AFTER DELETE ON `media_tags`
 FOR EACH ROW BEGIN
	UPDATE media_fulltext
		SET tags = TRIM(', ' FROM REPLACE(
			CONCAT(', ', tags, ', '),
			CONCAT(', ', (SELECT name FROM tags WHERE id = OLD.tag_id), ', '),
			', '))
		WHERE media_id = OLD.media_id;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `media_tags_ai`;
DELIMITER //
CREATE TRIGGER `media_tags_ai` AFTER INSERT ON `media_tags`
 FOR EACH ROW BEGIN
	UPDATE media_fulltext
		SET tags = CONCAT(tags, ', ', (SELECT name FROM tags WHERE id = NEW.tag_id))
		WHERE media_id = NEW.media_id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `migrate_version`
--

DROP TABLE IF EXISTS `migrate_version`;
CREATE TABLE IF NOT EXISTS `migrate_version` (
  `repository_id` varchar(250) NOT NULL,
  `repository_path` text,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `migrate_version`
--

INSERT INTO `migrate_version` (`repository_id`, `repository_path`, `version`) VALUES
('MediaCore Migrations', '/home/bengt/Studium/1313/PESS/src/mediacore-community/mediacore/migrations', 57);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(16) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  UNIQUE KEY `permission_name` (`permission_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `permissions`
--

INSERT INTO `permissions` (`permission_id`, `permission_name`, `description`) VALUES
(1, 'admin', 'Grants access to the admin panel'),
(2, 'edit', 'Grants access to edit site content'),
(3, 'view', 'View published media'),
(4, 'upload', 'Can upload new media'),
(5, 'MEDIA_UPLOAD', 'Grants the ability to upload new media');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `players`
--

DROP TABLE IF EXISTS `players`;
CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Daten für Tabelle `players`
--

INSERT INTO `players` (`id`, `name`, `enabled`, `priority`, `created_on`, `modified_on`, `data`) VALUES
(1, 'jwplayer', 1, 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}'),
(2, 'youtube', 1, 2, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{"showinfo": 0, "modestbranding": 1, "fs": 1, "autohide": 2, "hd": 0, "wmode": 0, "disablekb": 0, "version": 3, "rel": 0, "iv_load_policy": 1, "showsearch": 0, "autoplay": 0}'),
(3, 'vimeo', 1, 3, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}'),
(4, 'googlevideo', 1, 4, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}'),
(5, 'bliptv', 1, 5, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}'),
(6, 'dailymotion', 1, 6, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}'),
(7, 'flowplayer', 1, 7, '2013-10-30 21:19:35', '2013-10-31 03:18:47', '{}'),
(8, 'html5', 1, 8, '2013-10-30 21:19:35', '2013-10-31 03:18:48', '{}'),
(9, 'html5+flowplayer', 1, 9, '2013-10-30 21:19:35', '2013-10-31 03:18:49', '{"prefer_flash": false}'),
(10, 'sublime', 1, 10, '2013-10-30 21:19:35', '2013-10-31 03:18:50', '{"script_tag": ""}');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `podcasts`
--

DROP TABLE IF EXISTS `podcasts`;
CREATE TABLE IF NOT EXISTS `podcasts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `title` varchar(50) NOT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` text,
  `category` varchar(50) DEFAULT NULL,
  `author_name` varchar(50) NOT NULL,
  `author_email` varchar(50) NOT NULL,
  `explicit` tinyint(1) DEFAULT NULL,
  `copyright` varchar(50) DEFAULT NULL,
  `itunes_url` varchar(80) DEFAULT NULL,
  `feedburner_url` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=68 ;

--
-- Daten für Tabelle `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'email_media_uploaded', NULL),
(2, 'email_comment_posted', NULL),
(3, 'email_support_requests', NULL),
(4, 'email_send_from', 'noreply@localhost'),
(5, 'wording_user_uploads', '<p>Lade eine Mediendateien mit dem folgenden Formular auf den Server. Nach einer Prüfung wirst du von uns verständigt.</p>'),
(6, 'wording_administrative_notes', NULL),
(7, 'wording_display_administrative_notes', ''),
(8, 'popularity_decay_exponent', '4'),
(9, 'popularity_decay_lifetime', '36'),
(10, 'rich_text_editor', 'plain'),
(11, 'google_analytics_uacct', ''),
(12, 'featured_category', ''),
(13, 'max_upload_size', '314572800'),
(14, 'ftp_storage', 'false'),
(15, 'ftp_server', 'ftp.someserver.com'),
(16, 'ftp_user', 'username'),
(17, 'ftp_password', 'password'),
(18, 'ftp_upload_directory', 'media'),
(19, 'ftp_download_url', 'http://www.someserver.com/web/accessible/media/'),
(20, 'ftp_upload_integrity_retries', '10'),
(21, 'akismet_key', ''),
(22, 'akismet_url', ''),
(23, 'req_comment_approval', ''),
(24, 'use_embed_thumbnails', 'true'),
(25, 'api_secret_key_required', 'true'),
(26, 'api_secret_key', 'sFvCOzXblyZuROFInls'),
(27, 'api_media_max_results', '50'),
(28, 'api_tree_max_depth', '10'),
(29, 'general_site_name', 'Lernvideos'),
(30, 'general_site_title_display_order', 'append'),
(31, 'sitemaps_display', 'True'),
(32, 'rss_display', 'True'),
(33, 'vulgarity_filtered_words', ''),
(34, 'primary_language', 'de'),
(35, 'advertising_banner_html', ''),
(36, 'advertising_sidebar_html', ''),
(37, 'comments_engine', 'disabled'),
(38, 'facebook_appid', ''),
(39, 'appearance_logo', ''),
(40, 'appearance_background_image', ''),
(41, 'appearance_background_color', '#fff'),
(42, 'appearance_link_color', '#0f7cb4'),
(43, 'appearance_visited_link_color', '#0f7cb4'),
(44, 'appearance_text_color', '#637084'),
(45, 'appearance_navigation_bar_color', 'white'),
(46, 'appearance_heading_color', '#3f3f3f'),
(47, 'appearance_enable_cooliris', ''),
(48, 'appearance_display_login', 'True'),
(49, 'appearance_enable_featured_items', 'True'),
(50, 'appearance_enable_podcast_tab', ''),
(51, 'appearance_enable_user_uploads', 'True'),
(52, 'appearance_display_logo', ''),
(53, 'appearance_enable_widescreen_view', 'True'),
(54, 'appearance_display_background_image', ''),
(55, 'appearance_custom_css', ''),
(56, 'appearance_custom_header_html', ''),
(57, 'appearance_custom_footer_html', ''),
(58, 'appearance_custom_head_tags', ''),
(59, 'appearance_display_mediacore_footer', ''),
(60, 'appearance_display_mediacore_credits', ''),
(61, 'appearance_show_download', ''),
(62, 'appearance_show_share', ''),
(63, 'appearance_show_embed', ''),
(64, 'appearance_show_widescreen', ''),
(65, 'appearance_show_popout', ''),
(66, 'appearance_show_like', ''),
(67, 'appearance_show_dislike', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `settings_multi`
--

DROP TABLE IF EXISTS `settings_multi`;
CREATE TABLE IF NOT EXISTS `settings_multi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `storage`
--

DROP TABLE IF EXISTS `storage`;
CREATE TABLE IF NOT EXISTS `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `engine_type` varchar(30) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `display_name` (`display_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `storage`
--

INSERT INTO `storage` (`id`, `engine_type`, `display_name`, `enabled`, `created_on`, `modified_on`, `data`) VALUES
(1, 'LocalFileStorage', 'Local File Storage', 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{"path": null, "rtmp_server_uri": null}'),
(2, 'RemoteURLStorage', 'Remote URLs', 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{"rtmp_server_uris": []}'),
(3, 'YoutubeStorage', 'YouTube', 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}'),
(4, 'VimeoStorage', 'Vimeo', 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}'),
(5, 'BlipTVStorage', 'BlipTV', 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}'),
(6, 'DailyMotionStorage', 'Daily Motion', 1, '2013-10-30 21:19:35', '2013-10-30 21:19:35', '{}');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Trigger `tags`
--
DROP TRIGGER IF EXISTS `tags_ad`;
DELIMITER //
CREATE TRIGGER `tags_ad` AFTER DELETE ON `tags`
 FOR EACH ROW BEGIN
	UPDATE media_fulltext
		SET tags = TRIM(', ' FROM REPLACE(
			CONCAT(', ', tags,     ', '),
			CONCAT(', ', OLD.name, ', '),
			', '))
		WHERE media_id IN (SELECT media_id FROM media_tags
			WHERE media_id = OLD.id);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `tags_au`;
DELIMITER //
CREATE TRIGGER `tags_au` AFTER UPDATE ON `tags`
 FOR EACH ROW BEGIN
	IF OLD.name != NEW.name THEN
		UPDATE media_fulltext
			SET tags = TRIM(', ' FROM REPLACE(
				CONCAT(', ', tags,     ', '),
				CONCAT(', ', OLD.name, ', '),
				CONCAT(', ', NEW.name, ', ')))
			WHERE media_id IN (SELECT media_id FROM media_tags
				WHERE tag_id = OLD.id);
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(16) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `email_address` (`email_address`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `email_address`, `display_name`, `password`, `created`) VALUES
(1, 'admin', 'admin@example.com', 'Admin', '25a671e0c9854ea60671239f8ac25edc564e87390c8db92ab3ab4b709e2c7d38c18a3ada96fb4cdf', '2013-10-30 21:19:35'),
(2, 'schueler', 'schueler@example.com', 'Schüler', '4010877a41c78ad3a96d3892f9c114ed1390fbc07e7b32312c54af5ffcbdec6bbabf725a6cc5befc', '2013-10-31 03:22:11'),
(3, 'lehrer', 'lehrer@example.com', 'Lehrer', '598cd967d4b831e461df6ef6401f8a48ace6b9df376e75753c74e1f91b88e5530bf1e5fe71711648', '2013-10-31 03:23:17');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(3, 2);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `groups_permissions`
--
ALTER TABLE `groups_permissions`
  ADD CONSTRAINT `groups_permissions_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `groups_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`podcast_id`) REFERENCES `podcasts` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints der Tabelle `media_categories`
--
ALTER TABLE `media_categories`
  ADD CONSTRAINT `media_categories_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `media_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `media_files`
--
ALTER TABLE `media_files`
  ADD CONSTRAINT `media_files_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `media_files_ibfk_2` FOREIGN KEY (`storage_id`) REFERENCES `storage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `media_files_meta`
--
ALTER TABLE `media_files_meta`
  ADD CONSTRAINT `media_files_meta_ibfk_1` FOREIGN KEY (`media_files_id`) REFERENCES `media_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `media_meta`
--
ALTER TABLE `media_meta`
  ADD CONSTRAINT `media_meta_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `media_tags`
--
ALTER TABLE `media_tags`
  ADD CONSTRAINT `media_tags_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `media_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
